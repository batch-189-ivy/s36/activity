const Task = require('../models/Task.js')


module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error){
			return error
		} 
		return 'Task added successfully!'
	})
}

module.exports.findTask = (taskId) =>{
	return Task.findById(taskId).then((foundTask, error) => {
		if(error){
			return error
		}
		return foundTask
	})
}

module.exports.updateTask = (taskId, NewUpdatedTask) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			return error
		}
		result.name = NewUpdatedTask.name

		return result.save().then((updatedTask, error) => {
			if(error){
				return error
			}
			return updatedTask
		})
	})

}
