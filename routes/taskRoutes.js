const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()

router.post('/add', (req, res) => {
	TaskController.createTask(req.body).then((task) => res.send(task)) 
})

router.get('/:id', (req,res) =>{
	TaskController.findTask(req.params.id).then((foundTask) => res.send(foundTask))
})

router.put('/:id/complete', (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then((updatedTask) => res.send(updatedTask))
})


module.exports = router


