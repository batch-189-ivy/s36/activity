const express = require('express')
const mongoose = require('mongoose')
const taskRoutes = require('./routes/taskRoutes.js')

const app = express()
const port = 3001
app.use(express.json())
app.use(express.urlencoded({extended: true}))

mongoose.connect('mongodb+srv://ivybacudo:admin123@cluster0.merm9.mongodb.net/S36-Activity?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.on('error', () => console.error('Connection Error :('))
db.on('open', () => console.log('Connected to MongoDB!'))

// Routes
app.use('/tasks', taskRoutes)

app.listen(port, () => console.log(`Server is running at port ${port}`))


